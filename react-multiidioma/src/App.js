import React from "react";

import { withLocalize } from "react-localize-redux";
import { renderToStaticMarkup } from "react-dom/server";
import traducciones from "./translations/global.json";

import Algo from './Algo';
import Titulo from './Titulo';
import TriaIdioma from './TriaIdioma';
import Tradueix from "./Tradueix";

class App extends React.Component {

  constructor(props) {
    super(props);

    this.props.initialize({
      languages: [
        { name: "Català", code: "ca" },
        { name: "Castellà", code: "es" },
        { name: "Anglès", code: "en" },
      ],
      translation: traducciones,
      options: { renderToStaticMarkup, defaultLanguage: 'en' }
    });
  }

  render() {
   //importante! si activelanguage todavía no ha sido asignado mejor esperamos...
    if (!this.props.activeLanguage){
      return <h3>...</h3>;
    }

    let xxx = Tradueix.fes('algo');


    return (
      <>
        <TriaIdioma />
        <br />
        <h1>{xxx}</h1>
        <Titulo texto="global.hola" />
        <Algo idioma={this.props.activeLanguage.code}/>
      </>
    );
  }

}


export default withLocalize(App);