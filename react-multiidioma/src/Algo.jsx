import React from "react";
import { Translate } from "react-localize-redux";
import Tradueix from "./Tradueix";


class Algo extends React.Component {

        constructor(props){
                super(props);

                this.state = {
                        datos: []
                }
        }

        componentDidMount(){
                 //hacemos consulta a la bdd para obtener lista de contactos
                let url = "http://localhost:3000/api/contactos";
                fetch(url)
                .then(res => res.json())
                .then(datos => this.setState({datos:datos}))
                .catch(err => console.log(err));
        }


        render() {

                if (!this.state.datos.length){
                        return <h1>cargando datos...</h1>;
                }

                let xxx = Tradueix('algo');

               
                let idiomaActual = this.props.idioma;
                //mostraremos la columna "frase_xx" correspondiente al idioma actual
                let filas = this.state.datos.map( el => 
                        <tr><td>{el["frase_"+idiomaActual]}</td></tr>)

                return (
                        <>
                        <h1>{xxx}dsddd</h1>
                        <h2>
                                <Translate id="global.test" />
                        </h2>
                        <table>
                                {filas}
                        </table>
                        </>
                );
        }


}


export default Algo;

