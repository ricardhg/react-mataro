import React from "react";
import { Translate } from "react-localize-redux";

export default (props) => <h1><Translate id={props.texto} /></h1>;