import React from "react";
import { withLocalize } from "react-localize-redux";

import './css/language-toggle.css';

class TriaIdioma extends React.Component {

  render() {

    let languages = this.props.languages;
    // let activeLanguage = this.props.activeLanguage;

    return (
      <ul className="selector">
        {languages.map(lang => (
          <li key={lang.code}>
            <button onClick={() => this.props.setActiveLanguage(lang.code)}>
              {lang.name}
            </button>
          </li>
        ))}
      </ul>
    );
  }
}

export default withLocalize(TriaIdioma);

