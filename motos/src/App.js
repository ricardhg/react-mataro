import React from "react";
import Menu from './Menu';
import Moto from './Moto';

import foto_royal from './img/royal-enfield-interceptor-650-mark-three.png';
import foto_ducati from './img/ducati.jpg';
import foto_triumph from './img/triumph.png';

export default () => (
  <>
    <Menu />
    <Moto color = "red" marca="Royal Enfield" foto={foto_royal} descripcion="Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, fuga commodi. Excepturi reiciendis culpa distinctio, molestiae, adipisci inventore perferendis minus laborum labore est saepe ut modi fugiat facilis libero dolor!" />
    <Moto marca="Triumph" foto={foto_triumph} descripcion="Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, fuga commodi. Excepturi reiciendis culpa distinctio, molestiae, adipisci inventore perferendis minus laborum labore est saepe ut modi fugiat facilis libero dolor!" />
    <Moto marca="Ducati" foto={foto_ducati} descripcion="Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, fuga commodi. Excepturi reiciendis culpa distinctio, molestiae, adipisci inventore perferendis minus laborum labore est saepe ut modi fugiat facilis libero dolor!" />
  </>
);
