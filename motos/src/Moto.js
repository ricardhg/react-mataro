
import React from 'react';


export default class Moto extends React.Component {

    render() {

        
        return (
            <div style={{backgroundColor: this.props.color}} id={this.props.marca} class="moto">
                <h1>{this.props.marca}</h1>
                <p>{this.props.descripcion}</p>
                <img src={this.props.foto} />
            </div>
        );
    }

}

