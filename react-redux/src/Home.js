import React from "react";

import Mostrar from './components/Mostrar';
import Nuevo from './components/Nuevo';
import Eliminar from './components/Eliminar';


export default () => (
  <>
    <Nuevo />
    <Mostrar />
    <Eliminar />
  </>
);
