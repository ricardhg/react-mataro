import React from 'react';
import {connect} from 'react-redux';

class Mostrar extends React.Component {
    
    render() { 
        if (this.props.lista.length===0) {
            return <h3>No data...</h3>;
        }
        let i = 1;
        let lis = this.props.lista.map(el => <li key={i++}>{el}</li>);
        return ( 
            <>
                <ul>
                    {lis}
                </ul>
            </>
         );
    }
}
 
const mapStateToProps = (state) => {
    return {
       lista: state.elements
    }
  }
  
export default connect(mapStateToProps)(Mostrar);
  
  
