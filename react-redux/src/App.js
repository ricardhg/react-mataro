import React from "react";
import Home from './Home';
import Map from './Map';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import appReducer from '../reducers/appReducer'

import "./app.css";

const store = createStore(appReducer);

export default () => (
  <Provider store={store}>
    <Home />
    <hr />
    {/* <Map /> */}
    
  </Provider>
);
