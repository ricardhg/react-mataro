import React from "react";

import { Container, NavItem, Nav, NavLink } from 'reactstrap';



export default (props) => {
    let i = 0;
    let opcionesMenu = props.franjas.map(el => {
        return (
            <NavItem key={i++} >
                <NavLink href={"#" + el.idfranja} active >{el.idfranja}</NavLink>
            </NavItem>);
        });
    return (
        <>
            <div className="container">
                <Nav >
                    {opcionesMenu}
                </Nav>
            </div>
        </>
    )
} 