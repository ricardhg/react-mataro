
import React from 'react';
import { Container, Table } from 'reactstrap';


export default class Generos extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            generos : []
        };

    }

    componentDidMount(){
        let URL = "http://localhost:3000/api/genres";

        fetch(URL)
        .then(datos => datos.json())
        .then(generos => this.setState({generos: generos}))
        .catch(err => console.log(err));
    }

    render(){

        if(this.state.generos.length===0){
            return <h3>Cargando datos...</h3>;
        }
  
        let filas = this.state.generos.map(el => <tr key={el.id_genre}><td>{el.id_genre}</td><td>{el.genre}</td></tr>)

        return(
            <Container>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Genre</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                   
                </Table>
            </Container>
        );
    }

}