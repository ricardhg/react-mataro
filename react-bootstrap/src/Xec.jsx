

import React from 'react';

import './xec.css';

export default class Xec extends React.Component {


    render(){

        return (
            <label className="custom-control custom-checkbox">
                <input checked={this.props.value} name={this.props.name} onChange={this.props.onChange} type="checkbox" className="custom-control-input" />
                <span className="custom-control-indicator"></span>
                <span className="custom-control-text">{this.props.value ? this.props.onText : this.props.offText}</span>
            </label>
        );
    }
}
