import React from "react";
import Franja from './Franja';
import Tabla from './Tabla';
import Ordenadores from './Ordenadores';
import Generos from './Generos';
import Menu from './Menu';
import Xec from './Xec';
import { Container } from 'reactstrap';

const franjas = [
  { idfranja: "franja1", fondo: "red" },
  { idfranja: "franja2", fondo: "blue" },
  { idfranja: "franja3", fondo: "green" },
  { idfranja: "franja4", fondo: "yellow", letra: "black" },
];


export default class App extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      xec1: false
    }

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }



  render() {

    let i = 0;
    let elementos = franjas.map(el => <Franja key={i++} idfranja={el.idfranja} fondo={el.fondo} letra={el.letra} />);

    return (
      <>
        {/* <Menu franjas = {franjas} />
      <Container>
      {elementos}
      </Container> */}

        <br />
        <br />
        <br />
        {/* <h1>{ this.state.xec1 ? "ON" : "OFF"}</h1> */}
        
        <br />
        {/* <Xec onText="Activo" offText="Inactivo" value={this.state.xec1} name={"xec1"} onChange={this.handleInputChange} /> */}
        <Generos />
      </>
    );
  }

}