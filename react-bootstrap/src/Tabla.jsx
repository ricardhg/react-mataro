
import React from 'react';

import { Container, Table } from 'reactstrap';
import {CIUTATS, CIUTATS_CAT_20K} from './datos';

export default class Tabla extends React.Component {


    render() {

        let filas = CIUTATS_CAT_20K.filter(el => el.comarca==="Barcelonès").map(el => <tr>
            <td>{el.municipi}</td>
            <td>{el.poblacio}</td>
            <td>{el.comarca}</td>
            </tr>);

        return (
            <Container>
                <Table>
                    <thead>
                        <th>Municipi</th>
                        <th>Poblacio</th>
                        <th>Comarca</th>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                    
                </Table>
            </Container>
        );
    }
}
