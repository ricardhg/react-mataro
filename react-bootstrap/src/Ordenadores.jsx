
import React from 'react';

import { Container, Table } from 'reactstrap';


export default class Tabla extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dades: []
        }
    }


    componentDidMount() {
        let url = "http://localhost:3000/api/ordenadores";
        fetch(url)
            .then(dades => dades.json())
            .then(dades => {
                console.log(dades);
                this.setState({ dades });
            })
            .catch(err => console.log(err));
    }

    render() {

        if (!this.state.dades) {
            return <h3>Cargando datos...</h3>;
        }

        let i = 0;

        let filas = this.state.dades.map(el => <tr key={i++}>
            <td>{el.idordenadores}</td>
            <td>{el.modelo}</td>
            <td>{el.numero}</td>
        </tr>);

        return (
            <Container>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Modelo</th>
                            <th>Numero</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>

                </Table>
            </Container>
        );
    }
}
