import React from "react";
import { Container, Row, Col } from 'reactstrap';

export default class Franja extends React.Component{
  
    constructor(props){
        super(props);
    }

    render(){
        return (
                <Row  id={this.props.idfranja} style={{backgroundColor:this.props.fondo, color: this.props.letra || "white", padding:"40px 15px"}}>
                    <Col md="6" xs="12">
                        <h1>Container {this.props.idfranja}</h1>
                    </Col>
                    <Col className="col-12 col-md-6">
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse 
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui 
                        officia deserunt mollit anim id est laborum.
                        </p>
                    </Col>
                </Row>
        );
    }

}