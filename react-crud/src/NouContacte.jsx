
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

import { Titol, SeparadorY, SeparadorX} from './Util.js';


const API = "http://localhost:3000/api";


class EditCentre extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      nombre: '',
      email: '',
      ciudad: '',
      };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.tornar = this.tornar.bind(this);
    this.submit = this.submit.bind(this);
  }



  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }



  submit(e) {
    e.preventDefault();
    //VALIDACION...

    let contacto = {
      nombre: this.state.nombre,
      email: this.state.email,
      ciudad: this.state.ciudad,
    }


    fetch(API+'/contactos/', {
      method: 'POST', 
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify(contacto)
    })
    .then(respuesta => respuesta.json())
    .then(() => this.setState({ toList: true }))
    .catch(err => console.log(err))
    
  }


  tornar() {
    this.setState({ toList: true });
  }


  render() {

  
    if (this.state.toList) {
      return <Redirect to="/contactes" />
    }
    

    return (
      <>
        <Form onSubmit={this.submit}>
        <SeparadorY y="40px" />

          <Row>
          <Col><Titol>Nou contacte</Titol></Col>
            <Col>
              <span className="float-right">
                <Button type="button" onClick={this.tornar} className='' size='sm' color="danger" >{"Sortir sense desar"}</Button>
                <SeparadorX x="10px" />
                <Button type="submit" className='' size='sm' color="success" >{"Desar canvis"}</Button>
              </span>
            </Col>
          </Row>
          <SeparadorY y="20px" />
        

          <Row>
            <Col sm="6">
              <FormGroup>
                <Label for="nombreInput">Nom</Label>
                <Input required type="text" name="nombre" id="nombreInput"
                  value={this.state.nombre}
                  onChange={this.handleInputChange} />
              </FormGroup>
            </Col>

            <Col sm="6">
              <FormGroup>
                <Label for="emailInput">Email</Label>
                <Input type="email" name="email" id="emailInput"
                  value={this.state.email}
                  onChange={this.handleInputChange} />
              </FormGroup>
            </Col>

            <Col sm="6">
              <FormGroup>
                <Label for="ciudadInput">Ciutat</Label>
                <Input type="text" name="ciudad" id="ciudadInput"
                  value={this.state.ciudad}
                  onChange={this.handleInputChange} />
              </FormGroup>
            </Col>

          </Row>


        </Form>

      </>

    );
  }
}




export default EditCentre;




