
import React, { Component } from 'react';
import { Button, Row, Col, Table } from 'reactstrap';
import { Redirect } from 'react-router-dom';

import { Titol, SeparadorY, SeparadorX} from './Util.js';

//PREPARAT PER API XMYSQL!!!!!!!!!!!!!!!!!!
const API = "http://localhost:3000/api";


class LlistaContactes extends Component {

    constructor(props) {

        super(props);
        this.state = {
            aBorrar: false,
            aEditar: false,
            nouItem: false,
        }

        this.newItem = this.newItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.editItem = this.editItem.bind(this);
        this.loadData = this.loadData.bind(this);

    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
      
        fetch( API+"/contactos")
        .then(resp => resp.json())
        .then(dades => this.setState({dades: dades}))
        .catch(err=>console.log(err));
    }
    


    newItem() {
        this.setState({ nouItem: true })
    }

    deleteItem(itemId) {
        if (!itemId) return;
        fetch(API+"/contactos/"+itemId,  {method: 'DELETE'})
            .then(() => this.loadData())
            .catch(err => console.log(err))
    }

    editItem(itemId) {
        this.setState({ aEditar: itemId });
    }    
    

    render() {

        
        if (!this.state.dades){
            return (
                <>
                    <SeparadorY y="40px" />
                    <Titol>Carregant dades...</Titol>
                </>
            );
        } 


        if (this.state.nouItem) {
            return <Redirect to={"/nou-contacte"} />
        }

        if (this.state.aEditar) {
            return <Redirect to={"/edit-contacte/"+this.state.aEditar} />
        }

    


        const filesTaula = this.state.dades.map((el) =>
            <tr key={el.id}>
                <td>{el.id}</td>
                <td>{el.nombre}</td>
                <td>{el.email}</td>
                <td>{el.ciudad}</td>
                <td>
                    <i style={{cursor: "pointer"}} className='fa fa-lg fa-edit text-success' onClick={() => this.editItem(el.id)}></i>
                    <SeparadorX x="20px" />
                    <i style={{cursor: "pointer"}} className='fa fa-lg fa-trash text-danger' onClick={() => this.deleteItem(el.id)}></i>
                </td>
            </tr>
        );

        return (
            <>
                <SeparadorY y="40px" />
                <Row>
                    <Col><Titol>Llista de contactes</Titol></Col>
                    <Col><Button className='float-right' size='sm' color="primary" onClick={this.newItem}>Nou contacte</Button></Col>
                </Row>
                <SeparadorY y="20px" />
                <Row>
                    <Col>
                        <Table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nom</th>
                                    <th>Email</th>
                                    <th>Ciutat</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {filesTaula}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <br />


            </>
        );
    }
}



export default LlistaContactes;
