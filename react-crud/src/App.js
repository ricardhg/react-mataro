import React from "react";
import { BrowserRouter, NavLink, Link, Route, Switch } from 'react-router-dom';

import LlistaContactes from "./LlistaContactes.jsx";
import EditaContacte from './EditaContacte.jsx';
import NouContacte from './NouContacte.jsx';
import Home from './Home.jsx';

import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';


export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {isOpen: false,}
    this.toggle = this.toggle.bind(this);
  }


  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


    render(){
      return (

        <BrowserRouter>

          <Navbar color="primary" dark expand="md">
            <NavLink exact className="navbar-brand" to="/"><i className="fa fa-2x fa-users" ></i></NavLink>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    Contactes
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      <Link className="dropdown-item" to="/contactes">Llista de Contactes</Link>
                    </DropdownItem>
                    <DropdownItem>
                      <Link className="dropdown-item" to="/nou-contacte">Nou Contactes</Link>
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </Navbar>


          <Container>


            <Switch>

              <Route path="/contactes" component={LlistaContactes} />
              <Route path="/nou-contacte" component={NouContacte}  />
              <Route path="/edit-contacte/:itemId" component={EditaContacte} />

              <Route exact path="/" render={() => <Home  />} />

            </Switch>

          </Container>
        </BrowserRouter>
      );
    }

  }