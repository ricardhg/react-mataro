
//npm install node-fetch --save
const url = "http://api.citybik.es/v2/networks/bicing";

const fetch = require('node-fetch');
const minBicis = process.argv[2] * 1;
console.log(minBicis);

fetch(url)
.then(res => res.json())
.then( datos => datos.network.stations)
.then(estaciones => estaciones.filter(el => el.empty_slots>minBicis))
.then(filtradas => filtradas.forEach(el => console.log(el.empty_slots, el.name)))
.catch(err => console.log(err.message));