
const geo = require("./geometria.js");

const figura = process.argv[2];
const dato1 = process.argv[3];
const dato2 = process.argv[4];

if (figura==='c' || figura==='circulo'){
    console.log(`El area del circulo de radio ${dato1} es ${geo.areaC(dato1*1)}`);
    return;
}

if (figura==='t' || figura==='triangulo'){
    console.log(`El area del triangulo ${dato1}x${dato2} es ${geo.areaT(dato1*1,dato2*1)}`);
    return;
}

console.log("Error no te entiendo.");


