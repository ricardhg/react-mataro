const express = require('express')
const app = express()

app.use(express.urlencoded({ extended: true }));


app.get('/', function (req, res) {
  res.send('Hola mundo!')
})

app.get('/adios', function (req, res) {
    var nombre = req.query.nombre;
    res.send('Adios '+nombre)
  })
 
  app.get('/usuario', function (req, res) {
    var nombre = req.query.nombre;
    var password = req.query.password;
    res.send('Registrando usuario '+nombre+" con clave "+password);
  })

  app.post('/usuario', function (req, res) {
    // console.log(req.body);
    var nombre = req.body.nombre;
    var password = req.body.password;
    res.send('POST Registrando usuario '+nombre+" con clave "+password);
  })
 
  
app.listen(3001, function () {
  console.log('Escuchando en puerto 3001!')
})
