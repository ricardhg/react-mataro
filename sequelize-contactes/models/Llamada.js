'use strict';

module.exports = (sequelize, DataTypes) => {
  const Llamada = sequelize.define('Llamada', {
   
    resumen: DataTypes.STRING,
    idcontacto: {
      type: DataTypes.INTEGER,

      references: 'contactos', 
      referencesKey: 'id'
    }
    
  }, { tableName: 'llamadas', timestamps: true});
  
  return Llamada;
};


