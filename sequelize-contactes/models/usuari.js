
'use strict';

//definición de modelo SEQUELIZE


module.exports = (sequelize, DataTypes) => {
  const Usuari = sequelize.define('Usuari', {
    // id (como primary key y autoinc), createdAt y updatedAt son campos que sequelize ya incluye por defecto
    _uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    nom: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    admin: {
      type: DataTypes.BOOLEAN
    }
    
  }, { tableName: 'usuaris'});
  
  return Usuari;
};
