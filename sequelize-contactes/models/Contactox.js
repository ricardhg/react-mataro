'use strict';

/*
create view contactosx as select c.*, count(ll.idcontacto) numllamadas
 from contactos c left join llamadas ll on ll.idcontacto=c.id group by c.id;
 */

module.exports = (sequelize, DataTypes) => {
  const Contactox = sequelize.define('Contactox', {
   
    nombre: DataTypes.STRING,
    email: DataTypes.STRING,
    urlfoto: DataTypes.STRING,
    piefoto: DataTypes.STRING,
    ciudad: DataTypes.STRING,
    numllamadas: DataTypes.INTEGER,
  
    
  }, { tableName: 'contactosx', timestamps: true});
  
  return Contactox;
};


