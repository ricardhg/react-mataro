const express = require('express');
// const indexRouter = require('./routes/index-controller');
const generoRouter = require('./routes/contacto-controller');
const app = express();

app.use(express.json());

app.use(express.static('front'));

// app.use('/', indexRouter);
app.use('/genero', generoRouter);

const port = 3000;
app.listen(port, ()=>console.log("estamos en http://localhost:"+port));
