const express = require('express');

//bcrypt es un modulo que nos permite encriptar en una dirección
const bcrypt = require('bcrypt');

const model = require('../models/index');
const Usuari = model.Usuari;
const Token = model.Token;

//const tokenMaxMin = 30;

const router = express.Router();

// permitimos acceso desde otro servidor
router.all('/:algo',(req,res,next)=>{
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  next();
});



/* POST registro de usuario */
router.post('/registre', function(req, res, next) {
  // en la bdd siempre guardamos password encriptado
  // el número 10 es el número de "rounds" en el cálculo, cuanto mayor más tarda en computar pero más difícil es descubrir el password
  // https://stackoverflow.com/questions/46693430/what-are-salt-rounds-and-how-are-salts-stored-in-bcrypt
  const hash = bcrypt.hashSync(req.body.password, 10);
  //reemplazamos el password con su versión encriptada
  req.body.password=hash;
  // "create" es un método de sequelize, recibe un objeto con las propiedades/valores de los campos
  Usuari.create(req.body)
  .then( item => res.json({ok:true, data:item}))
  .catch((error)=>res.json({ok:false, error}))
});

/* POST LOGIN */
router.post('/login',  (req, res) => {
  //leemos nombre y password del body
  const { nom, password } = req.body;
  // const nom = req.body.nom;
  // const password = req.body.password;
  // si nombre / password no se han facilitado devolvemos error con código de estado 400
  if (!nom || !password) {
    return res.status(400).json({ok:false, error:"nombre o password no recibidos"});
  }

  //buscamos usuario y comprobamos si password coincide
  //findOne es un método de sequelize, si no encuentra nada devolverá error
  Usuari.findOne({ where: { nom } })
    .then((usuari) => {
      //comparamos el password recibido con el password del usuario guardado en bdd, ambos encriptados
      if (bcrypt.compareSync(password, usuari.password)) {
        //si ok, devolvemos usuario a siguiente "then" 
        return usuari;
      } else {
        // si no coinciden pasamos msg error a "catch"
        throw "password no coincide";
      }
    })
    .then((usuari)=>{
        //ok, login correcto, creamos un token aleatorio
        let token = '';
        const caractersPossibles = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const longitud = 15;
        for (var i = 0; i < longitud; i++) {
          token += caractersPossibles.charAt(
            Math.floor(Math.random() * caractersPossibles.length)
          );
        }
        //devolvemos un nuevo objeto "token" al siguiente then, que incluye id y nombre de usuario
        return Token.create({ token, idusuari: usuari._uuid, nomusuari: usuari.nom, admin:usuari.admin })
    })
    .then((token)=>res.json({ok:true, data:token})) //enviamos respuesta con el token completo en json
    .catch((error)=>res.json({ok:false, error:error}));

  });


  router.delete('/logout',  (req, res) => {
    const {token } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
      return res.status(400).json({ok:false, error:"token no recibido"});
    }
    // si lo recibimos, intentamos eliminarlo
    Token.destroy({ where: { token } })
    .then(()=>res.json({ok:true}))
    .catch((error)=>res.json({ok:false, error:error}));

  });

  router.post('/yo',  (req, res) => {
    const {token } = req.body;
    //si no hi ha token no acceptem
    if (!token) {
      return res.status(400).json({ok:false, error:"token no recibido"});
    }

    Token.findOne({ where: { token } })
    .then((token)=>{
      // caducidad del token??? de momento no miramos
      let ara = new Date();
      let diff = (ara.getTime() - token.createdAt.getTime()) / 60000;
      let mins = Math.floor(diff);
      let segs = Math.floor((diff-mins)*60);
      console.log(`Token: ${mins}'${segs}''`);
      return token;
    })
    .then((token)=>res.json({ok:true, data: token}))
    .catch((error)=>res.json({ok:false, error:error}));

  });

  router.post('/secret',  (req, res) => {
    const {token } = req.body;
    //si no hi ha token no acceptem
    if (!token) {
      return res.status(400).json({ok:false, error:"token no recibido"});
    }

    Token.findOne({ where: { token } })
    .then((token)=>{
      // caducidad del token??? de momento no miramos
      let ara = new Date();
      let diff = (ara.getTime() - token.createdAt.getTime()) / 60000;
      let mins = Math.floor(diff);
      let segs = Math.flooºr((diff-mins)*60);
      console.log(`Token: ${mins}'${segs}''`);
      
      return token;
    })
    .then((token)=>res.json({ok:true, msg: "La clave es "+token.token, data: token}))
    .catch((error)=>res.json({ok:false, error:error}));

  });


module.exports = router;