const express = require('express');
const router = express.Router();

const modelo = require('../models/index.js');


// router.all('/', (req,res,next) => {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
//     res.setHeader("Access-Control-Allow-Credentials", "true");
//     next();
// })

// router.all('/:xxx', (req,res,next) => {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
//     res.setHeader("Access-Control-Allow-Credentials", "true");
//     next();
// })

router.get('/', (req, res, next) => {
    modelo.Genero.findAll()
        .then(lista => res.json({ ok: true, data: lista }))
        .catch(err => res.json({ ok: false, error: err }));
});




router.post('/', (req, res, next) => {
    // let nombreGenero = req.body.genre;
    // modelo.Genero.create({genre: nombreGenero})
    modelo.Genero.create(req.body)
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});



router.get('/:id', (req, res, next) => {
    let idgenero = req.params.id;
    // modelo.Genero.findById(idgenero)
    modelo.Genero.findOne({ where: { id_genre: idgenero } })
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});


router.put('/:id', (req, res, next) => {
    let idgenero = req.params.id;
    // modelo.Genero.findById(idgenero)
    modelo.Genero.findOne({ where: { id_genre: idgenero } })
        .then(item => item.update(req.body))
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});



router.delete('/:id', (req, res, next) => {
    let idgenero = req.params.id;
    modelo.Genero.destroy({ where: { id_genre: idgenero } })
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});


module.exports = router;