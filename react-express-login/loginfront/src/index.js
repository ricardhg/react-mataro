import React from "react";
import ReactDOM from "react-dom";
import Root from "./Root";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

//https://medium.com/@faizanv/authentication-for-your-react-and-express-application-w-json-web-tokens-923515826e0#6563

ReactDOM.render(<Root />, document.getElementById("root"));
