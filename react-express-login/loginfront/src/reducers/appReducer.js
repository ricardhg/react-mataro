
const appReducer = (state = {
    email: false
}, action) => {

    let newState = JSON.parse(JSON.stringify(state));

    switch (action.type) {

        case 'SET_EMAIL':
            newState.email = action.value;
            return newState;

        case 'DELETE_EMAIL':
            newState.email = false;
            return newState;

        default:
            return state;
    }

}
export default appReducer;