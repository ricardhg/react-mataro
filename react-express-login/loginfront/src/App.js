import React from "react";
import Registre from './components/Registre';
import Login from './components/Login';
import Home from './components/Home';
import Navi from './components/Navi';
import Secret from './components/Secret';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import withAuth from './withAuth';

import { connect } from 'react-redux';


class App extends React.Component {

  render() {
    return (
        <BrowserRouter>
          <Container>
            <Navi email={this.props.email} />
            <Switch>
              <Route path="/login" component={Login} />
              <Route path="/registro" component={Registre} />
              <Route path="/secret" component={withAuth(Secret)} />
              <Route path="/" component={Home} />
            </Switch>
          </Container>
        </BrowserRouter>
    );
  }

}

const mapStateToProps = (state) => {
  return {
     email: state.email
  }
}

export default connect(mapStateToProps)(App);

