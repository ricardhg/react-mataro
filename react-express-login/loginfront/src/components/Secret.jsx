import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';

import {API} from '../config/Config';

export default class Secret extends React.Component {
    constructor() {
      super();
      //Set default message
      this.state = {
        message: 'Loading...'
      }
    }
    componentDidMount() {
      //GET message from server using fetch api
      fetch(API+'/secret',  {credentials: 'include'})
        .then(res => res.text())
        .then(res => this.setState({message: res}));
    }
    render() {
      return (
        <div>
            <br />
                <br />
          <h1>Secret</h1>
          <p>{this.state.message}</p>
          
        </div>
      );
    }
  }