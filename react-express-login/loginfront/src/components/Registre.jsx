import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

import { API } from '../config/Config';

export default class Registre extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nom: "",
            email: "",
            password: "",
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }


    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        let usuari = {
            nom: this.state.nom,
            email: this.state.email,
            password: this.state.password
        }
        fetch(API+'/register', {
            method: 'POST', 
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(usuari)
        })
        .then(respuesta => respuesta.json())
        .then(respuesta => {
            if (respuesta.ok===false){
            throw "ERROR REGISTRANT USUARI!";
            } else {
            console.log("OK USUARI REGISTRAT!!!");
            }
        })
      .catch(err => console.log(err));

    }

    render() {


        return (
            <Container>
                <br />
                <br />
                <h1>Registre</h1>
                <Form onSubmit={this.handleSubmit} >
                    <Row form>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="exampleEmail">Email</Label>
                                <Input required value={this.state.email} onChange={this.handleInputChange} type="email" name="email" id="exampleEmail" placeholder="entra email" />
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="examplePassword">Password</Label>
                                <Input value={this.state.password} onChange={this.handleInputChange} type="password" name="password" id="examplePassword" placeholder="entra password" />
                            </FormGroup>
                        </Col>
                    </Row>
                    <FormGroup>
                        <Label for="exampleNom">Nom complet</Label>
                        <Input required value={this.state.nom} onChange={this.handleInputChange} type="text" name="nom" id="exampleNom" placeholder="nom..." />
                    </FormGroup>
                
                  
                    <br />
                    <Button>Registrar</Button>
                </Form>

               

            </Container>
        );
    }
}