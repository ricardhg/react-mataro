import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
// import {withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';

import { API } from '../config/Config';
import { connect } from 'react-redux';


class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            toList: false,
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }



    
    //important:  credentials: 'include'
    handleSubmit(e) {
        e.preventDefault();
        let usuari = {
            email: this.state.email,
            password: this.state.password
        }
        fetch(API + '/login', {
            method: 'POST',
            credentials: 'include',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(usuari)
        })
            .then(res => {
                if (res.status === 200) {
                    return res;
                } else {
                    const error = new Error(res.error);
                    throw error;
                }
            })
            .then(res => res.json())
            .then(res => {
                let email = res.email;
                console.log("eureka! " + email);
                this.props.dispatch({
                    type: 'SET_EMAIL',
                    value: email,
                });
                this.props.history.push('/');
            })
            .catch(err => console.log(err));

    }


    render() {

        return (
            <>
                <br />
                <br />
                <h1>LOGIN</h1>
                <Form onSubmit={this.handleSubmit} >
                    <Row form>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="exampleEmail">Email</Label>
                                <Input required value={this.state.email} onChange={this.handleInputChange} type="email" name="email" id="exampleEmail" placeholder="entra email" />
                            </FormGroup>
                        </Col>
                        <Col md={6}>
                            <FormGroup>
                                <Label for="examplePassword">Password</Label>
                                <Input value={this.state.password} onChange={this.handleInputChange} type="password" name="password" id="examplePassword" placeholder="entra password" />
                            </FormGroup>
                        </Col>
                    </Row>
                    <br />
                    <Button>Login</Button>
                </Form>
            </>

        );
    }
}


// export default  withRouter(withCookies(Login));
export default connect()(withRouter(Login));