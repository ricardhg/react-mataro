import React from "react";

import App from './App';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import appReducer from './reducers/appReducer'
const store = createStore(appReducer);


class Root extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }

}


export default Root;

