const express = require('express');

//bcrypt es un modulo que nos permite encriptar en una dirección
const bcrypt = require('bcrypt');

const model = require('../models/index');
const Usuari = model.User;
const Token = model.Token;

const JWT = require('jsonwebtoken');
const SECRET = "merdolino";
const withAuth = require('../middleware');


//const tokenMaxMin = 30;

const router = express.Router();



/*important!
si tenim 2 servidors:
access-control-allow-headers ha d'incloure authorization
acces-control-allow-origin ha d'apuntar a la ip/servidor del front
el fetch del client (el login) ha d'incloure "credentials: true" a les propietats 
si és un sol servidor no cal!
*/
router.all('/', (req,res,next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
})

router.all('/:xxx', (req,res,next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
})


/* POST registro de usuario */
router.post('/register', function(req, res, next) {
  // en la bdd siempre guardamos password encriptado
  // el número 10 es el número de "rounds" en el cálculo, cuanto mayor más tarda en computar pero más difícil es descubrir el password
  // https://stackoverflow.com/questions/46693430/what-are-salt-rounds-and-how-are-salts-stored-in-bcrypt
  const hash = bcrypt.hashSync(req.body.password, 10);
  //reemplazamos el password con su versión encriptada
  req.body.password=hash;
  // "create" es un método de sequelize, recibe un objeto con las propiedades/valores de los campos
  Usuari.create(req.body)
  .then( item => res.json({ok:true, data:item}))
  .catch((error)=>res.json({ok:false, error}))
});

/* POST LOGIN */
router.post('/login',  (req, res) => {
  //leemos nombre y password del body
  const { email, password } = req.body;
  // const email = req.body.email;
  // const password = req.body.password;
  // si email / password no se han facilitado devolvemos error con código de estado 400
  if (!email || !password) {
    return res.status(400).json({ok:false, error:"email o password no recibidos"});
  }

  //buscamos usuario y comprobamos si password coincide
  //findOne es un método de sequelize, si no encuentra nada devolverá error
  Usuari.findOne({ where: { email } })
    .then( usuari => {
      //comparamos el password recibido con el password del usuario guardado en bdd, ambos encriptados
      if (bcrypt.compareSync(password, usuari.password)) {
        //si ok, devolvemos usuario a siguiente "then" 
        return usuari;
      } else {
        // si no coinciden pasamos msg error a "catch"
        throw "email o password incorrectes";
      }
    })
    .then( usuari => {

        const payload = { id: usuari.id, nom:usuari.nom, email:usuari.email };
        const token = JWT.sign(payload, SECRET, {
          expiresIn: '1h'
        });
        res.status(200).cookie('token', token, { httpOnly: true }).json({email: usuari.email});

    })
    .catch((error)=>res.status(401).json({ok:false, error:error}));

  });


  router.delete('/logout',  (req, res) => {
    

  });

  
  router.get('/check', withAuth,  (req, res) => {
    res.sendStatus(200);
  });


  router.get('/secret', withAuth, function(req, res) {
    res.send('The password is potato '+req.email);
  });


module.exports = router;