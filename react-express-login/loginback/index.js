//importamos/requerimos express y controladores
const express = require('express');
const userRouter = require('./routes/user-controller');
// const indexRouter = require('./routes/index-controller');
const cookieParser = require('cookie-parser')

//instanciamos nueva aplicación express
const app = express();

//cookies!
//https://www.npmjs.com/package/cookie-parser
app.use(cookieParser());


//necesario para poder recibir datos en json
app.use(express.json());

// las ruta "/" se gestiona en indexRouter
// app.use('/', indexRouter);
app.use( express.static('public'))
app.use('/api/user', userRouter);

//arranque del servidor
const port = 3000
app.listen(port, () => console.log(`App listening on port ${port}!`))
