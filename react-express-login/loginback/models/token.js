module.exports = (sequelize, DataTypes) => {
    const Token = sequelize.define('Token', {
  
      token: DataTypes.STRING,
      hores: DataTypes.INTEGER,
      iduser: DataTypes.INTEGER,
      nomuser: DataTypes.STRING,
      
    }, { tableName: 'tokens'});
    
    return Token;
  };
  