module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
  
      nom: DataTypes.STRING,
      email: {
          type: DataTypes.STRING,
          unique: true
        },
      password: DataTypes.STRING,
      
    }, { tableName: 'users'});
    
    return User;
  };
  