
const express = require('express');
const app = express();

app.use(express.urlencoded({ extended: true }));


app.get("/", function (req, res, next) {
    // let html = "";
    // html += '<form method="post">';
    // html += 'Valor inicial: <input name="valori" /><br>';
    // html += '<select name="desde"><option value="km">km</option><option value="millas">millas</option><option value="metros">metros</option><option value="yardas">yardas</option></select>'
    // html += '<select name="hacia"><option value="km">km</option><option value="millas">millas</option><option value="metros">metros</option><option value="yardas">yardas</option></select>'
    // html += '<br><button>Enviar</button>';
    // html += '</form>';
    // html += '<br><h3>Resultado: </h3>';

    res.render("inicio.ejs", {resultado: 0, valorInicial:0});
});

const tabla = {
    "km-metros": 1000,
    "km-millas": 1/1.6,
    "metros-km": 1/1000,
    "millas-km": 1.6,
}

app.post("/", function (req, res, next) {
    let valorInicial = req.body.valori;
    let desde = req.body.desde;
    let hacia = req.body.hacia;
    let conversion = desde + "-" + hacia;
    let factor = tabla[conversion];
    res.render("inicio.ejs", {resultado: valorInicial*factor, valorInicial: valorInicial});
});


app.listen(3001, function () {
    console.log("servidor inciado en http://localhost:3001");
})