import React from "react";
import {MOTOS} from './Motos';
import './css/Motos.css';
import {Table} from 'reactstrap';

export default class App extends React.Component {

render(){

  let i=0;
  let rows = MOTOS.map(el => <tr key={i++}><td>{el.model}</td><td>{el.preu}</td></tr>)

  return (
    <>
    
      <Table>
        <thead>
          <tr>
          <th>Model</th>
          <th>Preu</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </Table>
    </>
  );
}



}

