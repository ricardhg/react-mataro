import React from "react";

const API = "http://localhost:3000";

export default class App extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      data: []
    }
  }

  componentDidMount(){
    const url = API+"/genero";
    fetch(url)
    .then(data => data.json())
    .then(datajs => {
      if(datajs.ok) {
        this.setState({data: datajs.data})
      }
    })
    .catch(err => console.log(err));

  }

  render(){

    if (this.state.data.length === 0 ) {
      return <h3>Cargando datos...</h3>;
    }

    let datos = this.state.data.map(el => <li key={el.id_genre}>{el.genre}</li>)

    return (
      <>
        <ul>
          {datos}
        </ul>
      </>
    )
  }
}
