import React from 'react';


export default class Cosa extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            cosa: "algo",
            cosaSelect: "rojo",

        };

        this.cambia = this.cambia.bind(this);
        this.cambiaSelect = this.cambiaSelect.bind(this);
    }

    cambia(e){
        let valor = e.currentTarget.value;
        this.setState({
            cosa: valor
        })
        //console.log("valor asignado", valor, "state", this.state.cosa);
    }

    cambiaSelect(e){
        let valor = e.currentTarget.value;
        this.setState({
            cosaSelect: valor
        })
    }

    render(){
        return (
            <>
            <input value={this.state.cosa} onChange={this.cambia} />
            <h1>{this.state.cosa}</h1>

            <select value={this.state.cosaSelect} onChange={this.cambiaSelect} >
                <option value="verde">Color verde</option>
                <option value="rojo">Color rojo</option>
                <option value="azul">Color azul</option>
            </select>
            <h1>{this.state.cosaSelect}</h1>
            </>
        );
    }
}