import React from "react";
import './css/Componentes.css';

export const Titulo = (props) => <h1 className="principal">{props.texto}</h1>;

export const Subtitulo = (props) => <h2 className="principal">{props.children}</h2>;


