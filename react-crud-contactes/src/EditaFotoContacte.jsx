
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

import { Titol, SeparadorY, SeparadorX} from './Util.js';

import axios from 'axios';

// anterior xmysql
// xmysql -h localhost -d agenda -u root

import {API} from './Config';


class EditaFotoContacte extends Component {
  constructor(props) {
    super(props);

    this.state = { loading: false, selectedFile: false, itemId: this.props.match.params.itemId };

    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.tornar = this.tornar.bind(this);
    this.submit = this.submit.bind(this);

  }

  onChangeHandler = event=>{
    this.setState({
      selectedFile: event.target.files[0]
    })
  }

  submit(e) {
    e.preventDefault();

    const data = new FormData() 
    data.append('file', this.state.selectedFile);
    data.append('idcontacto', this.state.itemId);
    axios.post(API+"/contactos/foto", data)
    .then(res => { 
        console.log(res.statusText);
        this.setState({toList: true });
    })
  }

  tornar() {
    this.setState({ toList: true });
  }

  render() {

    if (this.state.loading){
        return (
            <>
                <SeparadorY y="40px" />
                <Titol>Carregant dades...</Titol>
            </>
        );
    } 

    if (this.state.toList) {
      return <Redirect to="/contactes" />
    }

    return (
      <>
        <Form onSubmit={this.submit} encType='multipart/form-data' >
          <SeparadorY y="40px" />
          <Row>
            <Col><Titol>Editar contacte</Titol></Col>
            <Col>
              <span className="float-right">
                <Button type="button" onClick={this.tornar} className='' size='sm' color="danger" >{"Sortir sense desar"}</Button>
                <SeparadorX x="10px" />
                <Button type="submit" className='' size='sm' color="success" >{"Desar canvis"}</Button>
              </span>
            </Col>
          </Row>
          <SeparadorY y="20px" />

          <Row>
            <Col sm="6">
              <FormGroup>
                <Label for="nombreFoto">Nom</Label>
                <Input type="file" name="file" onChange={this.onChangeHandler} id="nombreFoto" />
                <input type="hidden" name="idcontacto" value={this.state.itemId} />
              </FormGroup>
            </Col>
          </Row>
        </Form>
      </>
    );
  }
}


export default EditaFotoContacte;




