import React, { Component } from 'react';
import {withCookies } from 'react-cookie';
import { Redirect } from 'react-router-dom';
import {API} from './Config';
import {connect} from 'react-redux';
import {  Row, Col,  Button } from 'reactstrap';
import { EspaiDalt } from './Util';

class Logout extends Component {
    constructor() {
      super();
      //Set default message
      this.state = {
        username: '',
        password: '',
        tornar: false
      }
      this.logout = this.logout.bind(this);
    }

    logout() {
      let token =  this.props.cookies.get('rcc_token');
      fetch(API+'/usuaris/logout', {
        method: 'DELETE', 
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify({token})
      })
      .then(res => {
        console.log(res.json());
        //cal eliminar les cookies!
        this.props.cookies.remove("rcc_nomusuari");
        this.props.cookies.remove("rcc_idusuari");
        this.props.cookies.remove("rcc_token");
        this.props.dispatch({
          type:'LOGOUT'
          });
        this.setState({tornar: true})
      })
      .catch(err => console.log(err));
    }

  

    render() {

      
      if (this.state.tornar === true) {
        return <Redirect to='/' />
      }

      let token =  this.props.cookies.get('rcc_token');
      if (!token) {
        return <h2>Usuari no loguejat</h2>
      }


      return (
        <>
            <EspaiDalt />
   
            <Row className="justify-content-md-center">
            <Col xs="12" md="6" className="text-center">
              <Button onClick={this.logout} color="danger" className="btn-time btn-start btn-block "  >DESCONNECTAR - LOGOUT</Button>

            </Col>
          </Row>



        </>
      );
    }
  }


  export default withCookies(connect()(Logout));
  
