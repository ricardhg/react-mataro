
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

import { Titol, SeparadorY, SeparadorX} from './Util.js';

// xmysql -h localhost -d agenda -u root
import {API} from './Config';


class EditaContacte extends Component {
  constructor(props) {
    super(props);

    this.state = { loading: true };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.tornar = this.tornar.bind(this);
    this.submit = this.submit.bind(this);

    this.loadData = this.loadData.bind(this);

    this.loadData();

  }



  /*loadData*/
  loadData() {

    let itemId = this.props.match.params.itemId;
    fetch(API + "/contactos/" + itemId)
      .then(results => results.json())
      .then(data => {
        console.log(data);
        // return data[0]; //ull! per xmysql
        return data.data; //per nostra apis
      })
      .then(data => this.setState({
        id: data.id,
        nombre: data.nombre,
        email: data.email,
        ciudad: data.ciudad,
      }))
      .then(() => this.setState({ loading: false }))
      .catch(err => console.log(err));

  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }



  submit(e) {
    e.preventDefault();

    let contacto = {
      id: this.state.id,
      nombre: this.state.nombre,
      email: this.state.email,
      ciudad: this.state.ciudad,
    }

    fetch(API + '/contactos/'+this.state.id, {
      method: 'PUT',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify(contacto)
    })
      .then(respuesta => respuesta.json())
      .then(() => this.setState({ toList: true }))
      .catch(err => console.log(err))

  }


  tornar() {
    this.setState({ toList: true });
  }


  render() {

    if (this.state.loading){
        return (
            <>
                <SeparadorY y="40px" />
                <Titol>Carregant dades...</Titol>
            </>
        );
    } 

    if (this.state.toList) {
      return <Redirect to="/contactes" />
    }


    return (
      <>
        <Form onSubmit={this.submit}>
          <SeparadorY y="40px" />
          <Row>
            <Col><Titol>Editar contacte</Titol></Col>
            <Col>
              <span className="float-right">
                <Button type="button" onClick={this.tornar} className='' size='sm' color="danger" >{"Sortir sense desar"}</Button>
                <SeparadorX x="10px" />
                <Button type="submit" className='' size='sm' color="success" >{"Desar canvis"}</Button>
              </span>
            </Col>
          </Row>
          <SeparadorY y="20px" />

          <Row>
            <Col sm="6">
              <FormGroup>
                <Label for="nombreInput">Nom</Label>
                <Input type="text" name="nombre" id="nombreInput"
                  value={this.state.nombre}
                  onChange={this.handleInputChange} />
              </FormGroup>
            </Col>

            <Col sm="6">
              <FormGroup>
                <Label for="emailInput">Email</Label>
                <Input type="text" name="email" id="emailInput"
                  value={this.state.email}
                  onChange={this.handleInputChange} />
              </FormGroup>
            </Col>

            <Col sm="6">
              <FormGroup>
                <Label for="ciudadInput">Ciutat</Label>
                <Input type="text" name="ciudad" id="ciudadInput"
                  value={this.state.ciudad}
                  onChange={this.handleInputChange} />
              </FormGroup>
            </Col>

          </Row>


        </Form>

      </>

    );
  }
}




export default EditaContacte;




