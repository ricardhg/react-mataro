import React from "react";
import { BrowserRouter, NavLink, Link, Route, Switch } from 'react-router-dom';
import { CookiesProvider, withCookies } from 'react-cookie';

import LlistaContactes from "./LlistaContactes.jsx";
import EditaContacte from './EditaContacte.jsx';
import EditaFotoContacte from './EditaFotoContacte.jsx';
import NouContacte from './NouContacte.jsx';
import Home from './Home.jsx';

import Login from './Login';
import Logout from './Logout';
import Register from './Register';
import { connect } from 'react-redux';

import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';


class App extends React.Component {

  constructor(props) {
    super(props);

    let token = this.props.cookies.get('rcc_token');
    let nomusuari = ''; let idusuari = ''; let admin = '';

    if (token) {
      nomusuari = this.props.cookies.get("rcc_nomusuari");
      idusuari = this.props.cookies.get("rcc_idusuari");
      this.props.dispatch({
        type: 'LOGIN',
        nomusuari,
        idusuari,
        token
      });
    }


    this.state = { isOpen: false, }
    this.toggle = this.toggle.bind(this);
  }


  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


  render() {

    let logInOut;
    if (this.props.token) {
      logInOut = <NavItem><NavLink className="nav-link" to="/logout">Sortir</NavLink></NavItem>;
    } else {
      logInOut = (
        <>
          <NavItem><NavLink className="nav-link" to="/login">Entrar</NavLink></NavItem>
          <NavItem><NavLink className="nav-link" to="/register">Registre</NavLink></NavItem>
        </>
      );
    }

    let contactes = <></>;
    if (this.props.token) {
      contactes = (
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Contactes
                  </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <Link className="dropdown-item" to="/contactes">Llista de Contactes</Link>
            </DropdownItem>
            <DropdownItem>
              <Link className="dropdown-item" to="/nou-contacte">Nou Contactes</Link>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      );
    }

    return (

      <BrowserRouter>

        <Navbar color="primary" dark expand="md">
          <NavLink exact className="navbar-brand" to="/"><i className="fa fa-2x fa-users" ></i>{this.props.nomusuari}</NavLink>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {contactes}
              {logInOut}
            </Nav>
          </Collapse>
        </Navbar>


        <Container>


          <Switch>

            <Route path="/contactes" component={LlistaContactes} />
            <Route path="/nou-contacte" component={NouContacte} />
            <Route path="/edit-contacte/:itemId" component={EditaContacte} />
            <Route path="/edit-contacte-foto/:itemId" component={EditaFotoContacte} />

            <Route path="/login" component={Login} />
            <Route path="/logout" render={() => (<Logout />)} />
            <Route path="/register" render={() => (<Register cookies={this.props.cookies} />)} />

            <Route exact path="/" render={() => <Home />} />

          </Switch>

        </Container>
      </BrowserRouter>
    );
  }

}


const mapStateToProps = (state) => {
  return {
    nomusuari: state.nomusuari,
    idusuari: state.idusuari,
    token: state.token,
  }
}

export default withCookies(connect(mapStateToProps, null)(App));