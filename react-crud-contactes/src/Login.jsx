import React, { Component } from 'react';
import {withCookies } from 'react-cookie';
import { Redirect } from 'react-router-dom';
import {API} from './Config';
import {connect} from 'react-redux';

import {  Row, Col, Input, Button } from 'reactstrap';
import { EspaiDalt } from './Util';


class Login extends Component {
    constructor() {
      super();
      //Set default message
      this.state = {
        nom: '',
        password: '',
        tornar: false
      }
      this.submit = this.submit.bind(this);
      this.canvia = this.canvia.bind(this);
    }

    canvia(event) {
      const v =  event.target.value;
      const n = event.target.name;
      this.setState({
          [n]: v
      });
   }

    submit(e){
     
      e.preventDefault();
      let nom = this.state.nom;
      let password = this.state.password;
      let data = { nom, password };
      
      fetch(API+'/usuaris/login', {
        method: 'POST', 
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify(data)
      })
      .then(respuesta => respuesta.json())
      .then(respuesta => {
        if (respuesta.ok===false){
          throw respuesta.error;
        } else {
          return respuesta.data;
        }
      })
      .then(token => {
        console.log(token);
        if(token){
          console.log("establint cookies");
          this.props.cookies.set('rcc_nomusuari', token.nomusuari, {path: '/'});
          this.props.cookies.set('rcc_idusuari', token.idusuari, {path: '/'});
          this.props.cookies.set('rcc_token', token.token, {path: '/'});
          this.props.dispatch({
            type:'LOGIN',
            nomusuari: token.nomusuari,
            idusuari: token.idusuari,
            admin: token.admin,
            token: token.token
            });
          this.setState({tornar:true});
        }
      })
      .catch(err => console.log(err));
    }

  

    render() {

      if (this.state.tornar === true) {
          return <Redirect to='/' />
      }

      return (
        <>
          <EspaiDalt />
          <form className="login-form" onSubmit={this.submit}>

          <Row className="justify-content-md-center">
            <Col xs="12" md="6">
              <div className="caixa-mostra">
                <div>Nom</div>
                <div><Input onChange={this.canvia} type="text" name="nom" value={this.state.nom} placeholder="nom" /></div>
              </div>
              <br />
              <div className="caixa-mostra">
                <div>Password</div>
                <div><Input className="" onChange={this.canvia} type="password" name="password" value={this.state.password} placeholder="password" /></div>
              </div>
            </Col>
          </Row>
          <br />
          <br />

          <Row className="justify-content-md-center">
            <Col xs="12" md="6" className="text-center">
              <Button  type="submit" color="success" className="btn-time btn-start btn-block ">ENTRAR</Button>
            </Col>
          </Row>

          </form>


         
        </>
      );
    }
  }

  export default withCookies(connect()(Login));
  
