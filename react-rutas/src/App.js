import React from "react";
import { BrowserRouter, NavLink, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Table } from 'reactstrap';
import "./estilos.css";

import Home from './Home';
import Pagina1 from './Pagina1';
import Pagina2 from './Pagina2';

const articulos = [
  { id: 10, nombre: "zapatos" },
  { id: 11, nombre: "camisetas" },
  { id: 12, nombre: "gorras" },
  { id: 13, nombre: "pantalones" },
];


export default class App extends React.Component {

  render() {

    let filas = articulos.map(el => <tr><td><NavLink to={"/pagina2/" + el.id}>{el.id}</NavLink></td><td>{el.nombre}</td></tr>)

    return (
      <BrowserRouter>
        <Container>

          <Row>
            <Col>
              <ul>
                <li> <NavLink exact to="/">Home</NavLink> </li>
                <li> <NavLink to="/pagina1">Pagina1</NavLink> </li>
                <li> <NavLink to="/pagina2/xx">Pagina2</NavLink> </li>
              </ul>
            </Col>
          </Row>


          <Table>
            {filas}
          </Table>
          <div className="contenidos">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/pagina1" component={Pagina1} />
              <Route path="/pagina2/:idarticulo" component={Pagina2} />

            </Switch>
          </div>
        </Container>
      </BrowserRouter>
    );

  }
}
