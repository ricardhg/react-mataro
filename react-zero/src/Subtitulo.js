import React from "react";
import "./css/Subtitulo.css";

/*
export default (props) => (
    <h2 className="azul">{props.texto}</h2>
);
*/

class Subtitulo extends React.Component {
    render(){
        let nuevoTexto = this.props.texto.toUpperCase();
        return  <h2 className="azul">{nuevoTexto}</h2>;
        
    }
}

export default Subtitulo;
