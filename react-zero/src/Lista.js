
import React from 'react';
import './css/Lista';

export default class Lista extends React.Component {
    render(){
        let i = 0;
        let lis = this.props.datos.map(el => <li key={i++}>{el}</li>)
        return (
            <ul className="lista">
                {lis}
            </ul>
        )
    }
}