
import React from 'react';
import './css/Foto';

export default class Foto extends React.Component {
    render(){

        let imagen = `http://placekitten.com/${this.props.ancho}/${this.props.alto}`;
        return (
            <img className="marco" src={imagen} />
        )
    }
}