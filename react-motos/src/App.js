import React from "react";

import ducati from './img/ducati.jpg';
import triumph from './img/triumph.png';
import royal from './img/royal-enfield-interceptor-650-mark-three.png';

export default class App extends React.Component {
  render() {


    return (
      <>
      <ul className="nav">
        <li>Ducati</li>
        <li>Triumph</li>
        <li>Royal Enfield</li>
    </ul>


    <div id="ducati" className="moto">
        <h1>Ducati</h1>
        <img src={ducati} />
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, fuga commodi. Excepturi reiciendis culpa distinctio, molestiae, adipisci inventore perferendis minus laborum labore est saepe ut modi fugiat facilis libero dolor!</p>
    </div>
    

    <div id="triumph" className="moto">
        <h1>Triumph</h1>
        <img src={triumph} />
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, fuga commodi. Excepturi reiciendis culpa distinctio, molestiae, adipisci inventore perferendis minus laborum labore est saepe ut modi fugiat facilis libero dolor!</p>
    </div>
    
    
    <div id="re" className="moto">
        <h1>Royal Enfield</h1>
        <img src={royal} />
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, fuga commodi. Excepturi reiciendis culpa distinctio, molestiae, adipisci inventore perferendis minus laborum labore est saepe ut modi fugiat facilis libero dolor!</p>
    </div>

      </>
    );
  }
}
