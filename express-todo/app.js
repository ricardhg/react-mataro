const express = require('express');
const session = require('cookie-session'); 
const app = express();

//middleware necessari per poder llegir body en post
app.use(express.urlencoded({ extended: false }));

//middleware d'inicialització de les sessions 
app.use(session({secret: 'todotopsecret'}));


//aquest app use s'executa cada vegada que es rep una petició get/post...
//mirem la var todolist de la sessió, si no existeix en fem una de nova
//el next permet que es continui el codi
app.use(function(req, res, next){
    if (typeof(req.session.todolist) == 'undefined') {
        req.session.todolist = [];
    }
    next();
});


//definició ruta "get" principal
app.get('/todo', function(req, res) { 
    res.render('todo.ejs', {todolist: req.session.todolist, prueba: "hola que tal"});
});

//definició ruta post per afegir tasca
app.post('/todo/add/',  function(req, res) {
    if (req.body.novatasca != '') {
        //qualsevol canvi que fem a req.session modificarà la cookie al client!
        req.session.todolist.push(req.body.novatasca);
    }
    res.redirect('/todo');
});

//ruta get per eliminar un item
app.get('/todo/delete/:id', function(req, res) {
    if (req.params.id != '') {
        //llista.splice(3,1) elimina 1 element a partir de la posició 3 de la llista
        req.session.todolist.splice(req.params.id, 1);
    }
    //redirect reenvia a una altra url
    res.redirect('/todo');
});

//aquest use es genèric, el definim al final
//si no hi ha hagut cap coincidencia amb les rutes anteriors reenviem a /todo
app.use(function(req, res, next){
    res.redirect('/todo');
});

//inciem "escolta" en port 3000
app.listen(3000, function () {
    console.log('App a http://localhost:3000')
  });
  